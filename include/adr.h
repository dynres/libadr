#ifndef ADR_H__
#define ADR_H__


#include <mpi.h>

// just to avoid IDE warnings
typedef int MPI_Session;

/*
 * Dummy struct
 */
typedef struct ADR_Pset_struct
{
	MPI_Comm comm;
	/* ... */

} ADR_Pset;


#define ADR_PHASE_MALLEABLE_LOAD_BALANCING	0x1
#define ADR_PHASE_FINAL	0x2

#define ADR_TRUE	0x1
#define ADR_FALSE	0x0
#define ADR_NONE	-0x1
#define ADR_NULL	0x0

typedef int ADR_TYPE_BOOL


void ADR_init_apr_pset_from_mpi_session(
		const MPI_Session i_mpi_session,
		const char *i_custom_pset_name,
		ADR_Pset *o_apr_pset_phase
	);

void ADR_get_comm_from_pset(ession,
		ADR_Pset *i_apr_pset_phase,
		MPI_Comm *o_comm
	);



typedef struct
{
	// upper/lower limit + sweetspot
	int process_lower_limit = ADR_NONE;
	int process_upper_limit = ADR_NONE;
	int process_sweetspot = ADR_NONE;

	// scalability plot information
	double *process_scalability_plot_array = ADR_NULL;
	int process_scalability_plot_array_num_elements = ADR_NONE;

	// persistent across resource changes
	int persistent_across_resource_changes = ADR_NONE;
} ADR_opti_information;


void ADR_pset_set_opti_information(
		struct ADR_Pset *i_apr_pset,
		ADR_opti_information *i_opti_information
);



#define ADR_PHASE_MALLEABLE_LOAD_BALANCING 0x1
#define ADR_PHASE_FINAL 0x2

void ADR_set_phase_info(
	ADR_Pset *i_apr_pset_phase,
	int i_adr_phase

);



/*
 * Loop-based ADR
 */
void ADR_loop_phase_update_iteration_range(
		ADR_Pset *apr_pset,
		int *range_start,
		int *range_end
);

ADR_loop_phase_set_iteration_number(
		ADR_Pset *apr_pset,
		int iteration_current_id
);

ADR_TYPE_BOOL ADR_loop_phase_opti_information_requested(
		ADR_Pset *apr_pset
);


#endif
