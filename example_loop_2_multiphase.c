#include <mpi.h>



/*
 * Dummy struct
 */
struct APR_Pset
{
};


void data_setup(int N)
{

}


void app_specific_load_balancing(MPI_Comm comm_all, MPI_Comm comm_new, ...)
{
	/*
	 * Do load balancing
	 */
}


int main(int argc, const char *argv[])
{
	/*
	 * Iterative solver process:
	 * - Linear algebra (iterative solvers such as Jacobi, CG)
	 * - Time steppers
	 * - ...
	 */

	MPI_Session_init(...);

	char *session_pset = ...;

	/*
	 * Setup the different phases
	 *
	 * phase_id == -1 for initial processes at the beginning.
	 * APR_Pset data is automatically initialized.
	 */
	int phase_id;
	APR_Pset apr_pset_phases[3];

	DP_phase_init(apr_pset_phases, 3, phase_id);

	if (phase_id < 0)
	{
//		DP_apr_init_dependency(&apr_pset_phases[1], depends_on = &apr_pset_phases[0]);
		DP_apr_init_dependency(&apr_pset_phases[2], depends_on = &apr_pset_phases[1]);
	}

	phase_id = 0;

	if (phase_id <= 0)
	{
		/*
		 * Allocate some nodes a-priory, e.g., for preprocessing
		 */
		DP_resource_request(
				apr_pset_phase[0],
				/*
				process_lower_limit=5,
				process_upper_limit=5,
				*/
				number_of_processes = 5,
				num_cores_per_process = 1,
				resource_type = (GPU | CPU),
				blocking = true
			);


		DP_phase_set(&apr_pset_phase[0], MISC);
	}

	if (phase_id <= 1)
	{
		/*
		 * Initialize phase1 here
		 */
		DP_apr_pset_init(&apr_pset_phase[1], session_pset);


		/*
		 * Signal to resource manager to be able to cope with dynamic resources.
		 */

		if (1)
		{
			//DP_can_cope_with_changing_processes(pset, """ [RM blob] """);
			DP_loop_phase_opti_information(
					apr_pset_phase[1],
					process_lower_limit=5,
					process_upper_limit=100,
					process_sweetspot=50,
					persistent = true
				);
		}
		else
		{
			float process_scalability_plot[3] = {1, 1.9, 2.8, ...};
			DP_loop_phase_opti_information(
					apr_pset_phase[1],
					process_scalability_plot,
					num_process_scalability_plot = sizeof(process_scalability_plot)/sizeof(float),
					persistent = true
				);
		}
	}


	APR_Pset apr_pset_phase3;

	if (phase_id <= 2)
	{
		DP_loop_phase_opti_information(
				apr_pset_phase3,
				process_lower_limit=5,
				process_upper_limit=100,
				process_sweetspot=50,
				persistent = true
			);
	}

	/*
	 * Start actual computations
	 *
	 */

	if (phase_id == 0)
	{
		DP_phase_start(&apr_pset_phase[0]);

		MPI_Comm mpi_comm = apr_pset_phase[0].get_comm();

		/* Do some computations / communication */

		DP_phase_end(&apr_pset_phase[0]);
	}


	/*
	 * Setup code
	 */

	int iteration_total = 1000;
	int iteration_current_id;


	iteration_current_id = 0;

	MPI_Comm comm;
	DP_get_comm_from_pset(&apr_pset_phase[1], &comm);

	DR_loop_phase_init(apr_pset_phase[1]);

	// TODO
	bool apr_pset_primary_process;

	DR_loop_phase_set_iteration_range(range_start = iteration_current_id, range_max = iteration_total);
	for (; iteration_current_id < iteration_total; iteration_current_id++)
	{
		/*
		 * Signal that we're doing a new iteration
		 *
		 * More things can happen here, e.g.,
		 * - extraction and utilization of monitoring information
		 * - automatic computation of the new expected wallclock time
		 * -
		 */
		DR_loop_phase_new_iteration(&apr_pset, iteration_current_id);

		if (DR_loop_phase_opti_information_requested())
		{
			DP_loop_phase_opti_information(
					apr_pset,
					/* ... new optimization information here, e.g., scalability graphs ... */
				);
		}

		if (DR_resources_changed_and_available(apr_pset))
		{
			MPI_Comm comm_all;
			MPI_Comm comm_new;
			DR_get_new_comm(&comm_all, &comm_new);

			app_specific_load_balancing(apr_pset, comm_all, comm_new);

			comm = comm_new;

			DR_resource_change_finished(apr_pset);
		}

		/*
		 * Do some workload for time step 'timestep_nr'
		 */

		...

		/*
		 * Update resource optimization information
		 */
	}
	DR_loop_phase_finalize(apr_pset);


	/*
	 * More workload
	 */


	MPI_Session_finalize(...);

	return 0;
}
