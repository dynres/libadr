#include <mpi.h>
#include <adr.h>



int rank;
MPI_Comm comm;
char *pset;


void data_setup(int N)
{

}


void app_specific_load_balancing(MPI_Comm comm_all, MPI_Comm comm_new, ...)
{
	/*
	 * Do load balancing
	 */
}


int main(int argc, const char *argv[])
{
	/*
	 * Iterative solver process:
	 * - Linear algebra (iterative solvers such as Jacobi, CG)
	 * - Time steppers
	 * - ...
	 */

	int mpi_session;
	char *custom_pset_name;
	MPI_Session_init(...);

	ADR_Pset adr_pset_phase;
	ADR_init_adr_pset_from_mpi_session(mpi_session, custom_pset_name, &adr_pset_phase);

	MPI_Comm comm;
	ADR_get_comm_from_pset(&adr_pset_phase, &comm);


	ADR_set_phase_info(&adr_pset_phase, ADR_PHASE_MALLEABLE_LOAD_BALANCING);
	ADR_set_phase_info(&adr_pset_phase, ADR_PHASE_FINAL);


	/*
	 * Signal to resource manager to be able to cope with dynamic resources.
	 */
	ADR_opti_information i;

	if (1)
	{
		i.process_lower_limit = 5;
		i.process_upper_limit = 100;
		i.process_sweetspot = 50;

		i.persistent_across_resource_changes = ADR_TRUE;
	}
	else
	{
		double process_scalability_plot[3] = {1, 1.9, 2.8, ...};
		i.process_scalability_plot_array = process_scalability_plot;
		i.process_scalability_plot_array_num_elements = sizeof(process_scalability_plot)/sizeof(float);
		i.persistent_across_resource_changes = ADR_TRUE;
	}

	ADR_pset_set_opti_information(&adr_pset_phase, &i);

	/*
	 * Setup code
	 */
	int iteration_total = 1000;
	int iteration_current_id;


	iteration_current_id = 0;

	ADR_loop_phase_update_iteration_range(&adr_pset_phase, range_start = &iteration_current_id, range_end = &iteration_total);
	for (; iteration_current_id < iteration_total; iteration_current_id++)
	{
		/*
		 * Signal that we're doing a new iteration
		 *
		 * More things can happen here, e.g.,
		 * - extraction and utilization of monitoring information
		 * - automatic computation of the new expected wallclock time
		 */
		ADR_loop_phase_set_iteration_number(&adr_pset_phase, iteration_current_id);

		if (ADR_loop_phase_opti_information_requested(&adr_pset_phase))
		{
			/*
			 * preparing optimization information is done here.
			 *
			 * We can measure the time to do so here between the following and previous ADR_ API call.
			 */

			/*
			 * Once the resource optimization is done, we provide this information here.
			 */
			ADR_loop_phase_opti_information(
					&adr_pset,
					/* ... new optimization information here, e.g., scalability graphs ... */
				);
		}

		if (ADR_processes_changed_and_available(adr_pset))
		{
			MPI_Comm comm_all;
			MPI_Comm comm_new;

			app_specific_load_balancing(&adr_pset);

			ADR_get_comm_new(&adr_pset, &comm_new);
			comm = comm_new;

			int should_terminate;

#if 1
			should_terminate = ADR_process_change_finished(adr_pset);
#else
			ADR_info_get(&adr_pset, SHOULD_TERMINATE, &should_terminate);
#endif
			if (should_terminate)
				break;
		}

		/*
		 * Do some workload for time step 'timestep_nr'
		 */

		...

	}

	ADR_loop_phase_finalize(adr_pset);

	ADR_finalize();

	/*
	 * More workload
	 */
	MPI_Session_finalize(...);

	return 0;
}
