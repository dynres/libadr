

void app_specific_load_balancing(APR_Pset &apr_pset)
{
	/*
	 * Do load balancing within the
	 * communicator including new and old processes 
	 */
	 
 	MPI_Comm comm_all;
	ADR_get_comm_all(&apr_pset, &comm_all);

	APR_lb_pair *lb_information_recv;
	APR_lb_pair *lb_information_send;
	
	int lb_information_send_elements = 0;
	int lb_information_recv_elements = 0;
	
	/*
	 * Provide information on local workload
	 * and get information back where to
	 * send/recv data to/from.
	 *
	 * The workload itself is an integer here to ensure that the
	 * interval of workload to send/recv is also given by integer values.
	 *
	 * TODO: We might not need this. 
	 */
	ADR_load_balancing_init_workload_information(
		local_workload = int(56.3*1000),
		&lb_information_send,
		&lb_information_send_elements,
		&lb_information_recv,
		&lb_information_recv_elements
	);
	
	for (int i = 0; i < lb_information_send_elements; i++)
	{
		// Receiving rank
		rank_recv = lb_information_send_elements.rank_recv;
		
		// start integer of element to send
		start_int = lb_information_send_elements.start_id;
		
		// end integer of element to send
		start_int = lb_information_send_elements.start_id;

		MPI_Isend(...)
	}

	for (...)
	{
		MPI_Irecv(...)
	}
}
