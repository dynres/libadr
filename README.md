Awesome Dynamic Resource API (preliminary name)


Nomenclature, basic terms

* **Load balancing**: Move data around using just **one PSet**.

* **Migration**: Involves **two PSets**, moving data from one PSet to **another PSet** with the **same number of elements**.

* **Malleable**: Involves **two PSets** (potentially the same ones) with **different number of elements in them**.

* **Malleable migration**: **This can't exist**.

* **Malleable load balancing**: Move data to a **new PSet** with a **different number of elements**.

