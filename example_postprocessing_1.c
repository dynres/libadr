#include <mpi.h>
#include <adr.h>
#include <assert.h>


int main(int argc, const char *argv[])
{
	MPI_Session_init(...);

	/*
	 * Simulation runs
	 */

	MPI_Comm sim_comm;
	for (int i = 0,...)
	{

	}


	/*
	 * Postprocessing
	 *
	 * E.g.,
	 * - allocate I/O nodes which are extremely well connected or with large memory.
	 * - strong GPU for rendering task
	 *
	 * Here, we use ADR only after the simulation itself.
	 */

	int phase_id = 0;
	ADR_Pset apr_pset_postprocessing;
	ADR_init_apr_pset_from_mpi_session(mpi_session, custom_pset_name, &apr_pset_phase, &phase_id);

	if (phase_id == 0)
	{
		//ADR_set_phase_info(&apr_pset_phase, POSTPROCESSING_PHASE);
		ADR_set_phase_info(&apr_pset_phase, FINAL_PHASE);

		//ADR_can_cope_with_changing_processes(pset, """ [RM blob] """);

		ADR_opti_information(
				apr_pset_phase,
				memory_limit_lower = 5G,
				tags = "I/O,GPU",
				program = "/path/to/executable",
				program_phase_id = 1
			);

		/*
		 * Next, we inform the scheduler that we are willing to wait until we get an answer
		 */
		int success;
		ADR_opti_wait(
			blocking = true,	// wait until we get an answer
			can_fail = true,
			&success			//
		);

		if (success)
		{
			/*
			 * There are now dedicated resources available and we'll use them.
			 */
			MPI_Comm comm_new;
			ADR_get_comm_new(&apr_pset_postprocessing, &comm_new);

	#if 0
			/*
			 * The application can now copy the data on its own to the newly allocated nodes
			 */

	#else

			/*
			 * Or we can have ADR supporting this by, e.g., trying to spread the workload
			 * equally to the newly allocated resources
			 */
			ADR_Pset apr_pset_sim;
			ADR_init_apr_pset_from_comm(sim_comm, &apr_pset_sim);

			ADR_migration(
					apr_pset_sim,
					apr_pset_postprocessing,
					/* ... TODO ... */
				);

			int should_terminate = ADR_process_change_finished(&apr_pset_phase);

			if (should_terminate)
				goto finalize;
	#endif
		}
		else
		{
			/*
			 * Some fallback option, but we could simply
			 * continue with existing nodes to do the postprocessing
			 */
		}
	}

	phase_id++;

	assert(phase_id == 1);
	if (phase_id == 1)	// is actually obsolete since we should always be in phase_id==1
	{
		/*
		 * Do the postprocessing in here
		 */
	}


finalize:
	ADR_finalize(&apr_pset_phase);

	/*
	 * More workload
	 */

	// finalize the MPI Session
	MPI_Session_finalize(...);

}
